package com.dcp.musicsearcher.activity.search.list;


import com.dcp.musicsearcher.api.pojo.songs.SongSearch;

import retrofit2.Response;

public interface AsyncSearchRequestCallback {
    void onSearchRequestReturn(Response<SongSearch> response);
    void startLoad();
    void endLoad();
    void errorLoad();
}
