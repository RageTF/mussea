package com.dcp.musicsearcher.activity.search.list.item;

import android.support.v7.app.AppCompatActivity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dcp.musicsearcher.R;
import com.dcp.musicsearcher.activity.search.MainActivity;
import com.dcp.musicsearcher.data.FavoritesController;
import com.github.clans.fab.FloatingActionButton;

public class ItemActivity extends AppCompatActivity implements TaskInterface,View.OnClickListener {

    private TextView mItemPerformer;
    private TextView mItemName;
    private TextView mItemLyrics;
    private ProgressBar mProgressBar;
    private long mItemId;
    private boolean mLoadVisible;

    private FloatingActionButton btnToFavorites;
    private FloatingActionButton btnSearchInfo;

    private Button mRefreshButton;
    private FavoritesController mFavoritesController;
    private boolean mRefreshVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        mFavoritesController=new FavoritesController(this);
        mItemPerformer = (TextView) findViewById(R.id.item_performer);
        mItemPerformer.setTypeface(MainActivity.typeface);
        mItemPerformer.setSelected(true);
        mItemLyrics = (TextView) findViewById(R.id.item_lyrics);
        mItemLyrics.setTypeface(MainActivity.typeface);
        mItemName= (TextView) findViewById(R.id.item_name);
        mItemName.setTypeface(MainActivity.typeface);
        mItemName.setSelected(true);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);

        btnSearchInfo = (FloatingActionButton) findViewById(R.id.item_info);
        btnSearchInfo.setOnClickListener(this);

        btnToFavorites = (FloatingActionButton) findViewById(R.id.to_favorites_button);
        btnToFavorites.setOnClickListener(this);

        mRefreshButton= (Button) findViewById(R.id.refresh_button);
        mRefreshButton.setTypeface(MainActivity.typeface);
        mRefreshButton.setOnClickListener(this);

        mLoadVisible=savedInstanceState==null?true:savedInstanceState.getBoolean("loadVisible");

        if(mLoadVisible){
            mProgressBar.setVisibility(View.VISIBLE);
        }else{
            mProgressBar.setVisibility(View.GONE);
        }
        mRefreshVisible=false;


        if(savedInstanceState!=null){
            mRefreshVisible=savedInstanceState.getBoolean("refreshVisible");
            mItemName.setText(savedInstanceState.getString("itemName"));
            mItemPerformer.setText(savedInstanceState.getString("itemPerformer"));
            mItemLyrics.setText(savedInstanceState.getString("itemLyrics"));
        }
        if(mRefreshVisible){
            mRefreshButton.setVisibility(View.VISIBLE);
        }else{
            mRefreshButton.setVisibility(View.GONE);
        }


        Intent intent = getIntent();
        mItemName.setText(intent.getStringExtra("KEY_NAME_SONG"));
        mItemPerformer.setText(intent.getStringExtra("KEY_NAME_ARTIST"));
        mItemId=intent.getLongExtra("KEY_ID",0);

        if(mFavoritesController.contains(mItemId)){
            btnToFavorites.setImageResource(R.mipmap.ic_star_white_24dp);
        }else{
            btnToFavorites.setImageResource(R.mipmap.ic_star_outline_white_24dp);
        }

        getFragment();
    }


    private ItemFragment getFragment(){
        ItemFragment fragment = (ItemFragment) getSupportFragmentManager().findFragmentByTag(ItemFragment.class.getName());
        if(fragment == null){
            fragment = ItemFragment.newInstance(mItemId);
            getSupportFragmentManager().beginTransaction().add(fragment, ItemFragment.class.getName()).commit();
        }
        return fragment;
    }

    @Override
    public void onTaskStart() {
        mProgressBar.setVisibility(View.VISIBLE);
        mLoadVisible=true;
    }

    @Override
    public void onFinish(String s) {
        mProgressBar.setVisibility(View.GONE);
        mItemLyrics.setText(s);
        mLoadVisible=false;
    }

    @Override
    public void onError() {
        mRefreshButton.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mRefreshVisible=true;
        mLoadVisible=false;
        Toast.makeText(getApplicationContext(),
                "No Internet connection", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("itemName", mItemName.getText().toString());
        outState.putString("itemPerformer", mItemPerformer.getText().toString());
        outState.putString("itemLyrics", mItemLyrics.getText().toString());
        outState.putBoolean("loadVisible",mLoadVisible);
        outState.putBoolean("refreshVisible",mRefreshVisible);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.item_info:
                String itemName=mItemName.getText().toString();
                String itemPerformer=mItemPerformer.getText().toString();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q="+itemName+" "+itemPerformer+"&c=music")));
                break;

            case R.id.to_favorites_button:
                if(mFavoritesController.contains(mItemId)){
                    mFavoritesController.removeFromFavorites(mItemId);
                    btnToFavorites.setImageResource(R.mipmap.ic_star_outline_white_24dp);
                }else {
                    mFavoritesController.addToFavorites(mItemId, mItemName.getText().toString(), mItemPerformer.getText().toString());
                    btnToFavorites.setImageResource(R.mipmap.ic_star_white_24dp);
                }
                break;

            case R.id.refresh_button:
                mRefreshButton.setVisibility(View.GONE);
                mRefreshVisible=false;
                getFragment().start();
                break;
        }
    }
}
