package com.dcp.musicsearcher.activity.search.list;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.dcp.musicsearcher.api.ApiRequester;
import com.dcp.musicsearcher.api.pojo.songs.SongSearch;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by Ruslan on 14.01.2017.
 */

public class RetainFragment extends Fragment {
    private AsyncSearchRequestCallback mRequestCallback;
    private OnSearchRequestAsyncTask mOnSearchAsyncTask;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRequestCallback = (AsyncSearchRequestCallback) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mRequestCallback = (AsyncSearchRequestCallback) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void startAsync(String... params) {
        if(mOnSearchAsyncTask==null) {
            mOnSearchAsyncTask = new OnSearchRequestAsyncTask();
            mOnSearchAsyncTask.execute(params);
        }
    }

    private class OnSearchRequestAsyncTask extends AsyncTask<String, Void, Response<SongSearch>> {

        @Override
        protected void onPreExecute() {
            mRequestCallback.startLoad();
        }

        @Override
        protected Response<SongSearch> doInBackground(String... params) {
            ApiRequester apiRequester = ApiRequester.getInstance();
            ApiRequester.SongSearchBuilder searchBuilder = new ApiRequester.SongSearchBuilder();
            Response<SongSearch> response=null;

            try {
                searchBuilder.setNameArtist(params[0])
                        .setNameSong(params[1])
                        .setAnyWordInLyrics(params[2]);
                response = apiRequester.getResultSongSearch(searchBuilder);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return response;
        }


        @Override
        protected void onPostExecute(Response<SongSearch> response) {
            mOnSearchAsyncTask=null;
            if(response==null){
                mRequestCallback.errorLoad();
            }else {
                mRequestCallback.onSearchRequestReturn(response);
                mRequestCallback.endLoad();
            }
        }

    }
}
