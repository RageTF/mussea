package com.dcp.musicsearcher.activity.search.list;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dcp.musicsearcher.R;
import com.dcp.musicsearcher.activity.search.MainActivity;
import com.dcp.musicsearcher.activity.search.list.item.ItemActivity;
import com.dcp.musicsearcher.api.pojo.songs.SongSearch;
import com.dcp.musicsearcher.api.pojo.songs.TrackList;

import java.util.List;

import retrofit2.Response;

public class TrackListActivity extends AppCompatActivity implements AsyncSearchRequestCallback, OnTrackItemClickListener {

    private TextView mIsEmpty;
    private RecyclerView mTrackListRecyclerView;
    private ProgressBar mLoadProgressBar;
    private Button mRefresh;
    private TrackListAdapter mTrackListAdapter;
    private RetainFragment mRetainFragment;

    private static List<TrackList> mData;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("KEY_SAVE", "NOT_NULL");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_list);

        final String nameArtistSearch = getIntent().getStringExtra("KEY_NAME_ARTIST");
        final String nameSongSearch = getIntent().getStringExtra("KEY_NAME_SONG");
        final String wordsSearch = getIntent().getStringExtra("KEY_WORDS");

        mIsEmpty = (TextView) findViewById(R.id.is_empty);
        mIsEmpty.setTypeface(MainActivity.typeface);
        mIsEmpty.setVisibility(View.GONE);

        mLoadProgressBar = (ProgressBar) findViewById(R.id.pb_track_list_update);
        mLoadProgressBar.setVisibility(View.GONE);

        mTrackListAdapter = new TrackListAdapter();
        mTrackListAdapter.setOnTrackItemClickListener(this);
        mTrackListRecyclerView = (RecyclerView) findViewById(R.id.rv_track_list);
        mTrackListRecyclerView.setAdapter(mTrackListAdapter);
        mTrackListRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRefresh = (Button) findViewById(R.id.refresh_button);
        mRefresh.setTypeface(MainActivity.typeface);
        mRefresh.setVisibility(View.GONE);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRetainFragment.startAsync(nameArtistSearch, nameSongSearch, wordsSearch);
                mRefresh.setVisibility(View.GONE);
            }
        });

        if (savedInstanceState == null) {
            mData = null;
            mRetainFragment = getRetainFragment();
            mRetainFragment.startAsync(nameArtistSearch, nameSongSearch, wordsSearch);
        }

    }

    private RetainFragment getRetainFragment() {

        RetainFragment retainFragment = (RetainFragment) getFragmentManager().findFragmentByTag(RetainFragment.class.getName());

        if (retainFragment == null) {
            retainFragment = new RetainFragment();
            getFragmentManager()
                    .beginTransaction()
                    .add(retainFragment, RetainFragment.class.getName())
                    .commit();
            getFragmentManager().executePendingTransactions();
        }
        return retainFragment;
    }

    @Override
    public void startLoad() {
        mLoadProgressBar.setVisibility(View.VISIBLE);
        mTrackListRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onSearchRequestReturn(Response<SongSearch> response) {
        if (response.isSuccessful()) {
            if (response.body().getMessage().getBody().getTrackList().isEmpty()) {
                mIsEmpty.setVisibility(View.VISIBLE);
                return;
            }
            mData = response.body().getMessage().getBody().getTrackList();
            mTrackListAdapter.notifyDataSetChanged();
        } else {
            mIsEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void endLoad() {
        mLoadProgressBar.setVisibility(View.GONE);
        mTrackListRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void errorLoad() {
        Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
        mRefresh.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickListener(int position) {

        Intent intent = new Intent(TrackListActivity.this, ItemActivity.class);
        intent.putExtra("KEY_NAME_ARTIST", mData.get(position).getTrack().getArtistName());
        intent.putExtra("KEY_NAME_SONG", mData.get(position).getTrack().getTrackName());
        intent.putExtra("KEY_ID", mData.get(position).getTrack().getTrackId());

        startActivity(intent);

    }

    private class TrackListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private OnTrackItemClickListener onTrackItemClickListener;

        public void setOnTrackItemClickListener(OnTrackItemClickListener onTrackItemClickListener) {
            this.onTrackItemClickListener = onTrackItemClickListener;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_track, null);
            return new TrackViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((TrackViewHolder) holder).bind(position);
        }

        @Override
        public int getItemCount() {
            return mData == null ? 0 : mData.size();
        }

        private class TrackViewHolder extends RecyclerView.ViewHolder {

            private TextView tvSongName;
            private TextView tvArtist;

            public TrackViewHolder(View itemView) {
                super(itemView);

                tvSongName = (TextView) itemView.findViewById(R.id.tv_song_name);
                tvSongName.setTypeface(MainActivity.typeface);
                tvArtist = (TextView) itemView.findViewById(R.id.tv_artist);
                tvArtist.setTypeface(MainActivity.typeface);
            }

            void bind(final int position) {
                tvSongName.setText(mData.get(position).getTrack().getTrackName());
                tvArtist.setText(mData.get(position).getTrack().getArtistName());
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onTrackItemClickListener.onClickListener(position);
                    }
                });
            }
        }
    }
}
