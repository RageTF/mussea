package com.dcp.musicsearcher.activity.search.list;

public interface OnTrackItemClickListener {
    void onClickListener(int position);
}
